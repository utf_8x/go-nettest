package main

import (
	"net"
	_ "bytes"
	log "github.com/sirupsen/logrus"
)

func beat(c net.Conn) {
	var b = []byte{byte(1)}
	var r [1024]byte

	_, err := c.Read(r[:])

	var responseLength = r[0]
	log.Info(responseLength)
	var response = r[2:responseLength+2]

	if err != nil {
		log.Warn("Empty payload")
	} else {		
		log.WithFields(log.Fields{
			"server": string(response),
		}).Info("Received heartbeat")

		c.Write(b)
		c.Close()
	}
}

func main() {
	l, err := net.Listen("tcp", "localhost:1235")

	log.Info("Starting heartbeat monitor")
	log.Info("==========================")

	if err != nil {
		log.Error("Can't open port")
	}

	defer l.Close()

	for {
		conn, err := l.Accept()

		if err != nil {
			log.Error("Connection refused")
		} else {
			go beat(conn)
		}
	}
}
