package main

import (	
	"net"
	"sync"
	"time"
	"os"
	log "github.com/sirupsen/logrus"
)

func request(num int, wg *sync.WaitGroup) {
	
	log.WithFields(log.Fields{
		"requestNumber": num,
		}).Info("Dispatching asynchronous request.")


	conn, err := net.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Error("Connection refused")
	} else {
		var b [1]byte
		n, err := conn.Read(b[:])
		if err != nil {
			log.Warn("No response")
		} else {

			log.WithFields(log.Fields{
				"responseContent": b,
				"responseLength": n,
			}).Info("Received byte(s)")
		}
	}

	wg.Done()
}

func init() {
	log.SetOutput(os.Stdout)	
}

func main() {
	var wg sync.WaitGroup
	start := time.Now()
	
	log.Info("Dispatching requests")
	log.Info("=========================")

	ix := 10

	wg.Add(ix)

	for i := 0; i < ix; i++ {
		go request(i, &wg)
	}	

	wg.Wait()
	elapsed := time.Since(start)

	log.Info("=========================")

	log.WithFields(log.Fields{
		"elapsedTime": elapsed,
	}).Info("Done")	
}
