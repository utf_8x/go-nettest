package main

import (
	"net"	
	"sync/atomic"
	"time"
	_ "math/rand"
	log "github.com/sirupsen/logrus"
)

func heartbeat() {
	log.Info("Heartbeat sender initialized")
	for {
		log.Info("Heartbeat sender increment")
		conn, err := net.Dial("tcp", "localhost:1235")
		if err != nil {
			log.Error("Could not send heartbeat (connection refused)")
		} else {
			var b [1]byte
			var serverName = []byte("Server 1 👍")
			var c = append([]byte{byte(len(serverName)), 0x0}, serverName...)
			log.Info(c)			
			conn.Write(c)
			_, err := conn.Read(b[:])
			if err != nil {
				log.Warn("Empty heartbeat response")
			} else {
				log.WithFields(log.Fields{
					"heartBeatResponse": b,
				}).Info("Heartbeat sent")
			}
		}

		time.Sleep(1 * time.Second)
	}
}

func request(c net.Conn, ops *uint64) {
		start := time.Now()
		atomic.AddUint64(ops, 1)
		opNumber := atomic.LoadUint64(ops)

		var b = []byte{byte(opNumber)}
		
		log.WithFields(log.Fields{
			"requestNumber": opNumber,
		}).Info("Received a request...")

		
		//time.Sleep(time.Duration(rand.Intn(5)) * time.Second)
				

		c.Write(b)
		c.Close()

		duration := time.Since(start)

		log.WithFields(log.Fields{
			"requestDuration": duration,
			"reqestNumber": opNumber,
		}).Info("Completed a request")
}

func main() {
	var ops uint64
	l, err := net.Listen("tcp", "localhost:1234")
	log.Info("Starting TCP server at localhost:1234")
	log.Info("=====================================")

	go heartbeat()

	if err != nil {
		log.Error("Can't open port")
	}
	defer l.Close()
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Error("Connection refused")
		} else {

			go request(conn, &ops)			
		}
	}
}
